Data repository of the research group led by [Dr. Jacek Herbrych](https://jacekherbrych.github.io) (Wrocław University of Science and Technology)

The repository have been permanently moved to: [https://github.com/jacekherbrych/DataRepository](https://github.com/jacekherbrych/DataRepository)